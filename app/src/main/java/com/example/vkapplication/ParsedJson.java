package com.example.vkapplication;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by А on 22.06.2016.
 */
public class ParsedJson {
    public static final String LOG_TAG = "VKParsedJson";
    boolean isRepost;
    int historyPostID;
    int historyPostOwnerID;
    String message;
    String type;
    int attachmentOwnerID;
    int mediaID;
    String attachmentsPostI = "";
    String attachmentsPost = "";
    int postID;

    public static ParsedJson fromJson(JSONObject json) throws JSONException{
        ParsedJson parsedJson = new ParsedJson();
        try {
            parsedJson.postID = json.getInt("id");
            parsedJson.message = json.getString("text");
            Log.d(LOG_TAG, "message: " + parsedJson.message);
            JSONArray attachments = json.optJSONArray("attachments");
            if (attachments != null) {
                int attacmentsNumber = attachments.length();
                parsedJson.attachmentsPost = "";
                for (int m = 0; m < attacmentsNumber; m++) {
                    Log.d(LOG_TAG, "m = " + m);
                    JSONObject attachmentI = attachments.optJSONObject(m);
                    parsedJson.type = attachmentI.getString("type");
                    if (parsedJson.type.equals("photos_list")) {
                        continue;
                    }
                    if(parsedJson.type.equals("link")){
                        return null;
                    }
                    JSONObject iAttachment = attachmentI.getJSONObject(parsedJson.type);
                    parsedJson.attachmentOwnerID = Integer.parseInt(iAttachment.getString("owner_id"));
                    parsedJson.mediaID = Integer.parseInt(iAttachment.getString("id"));
                    parsedJson.attachmentsPostI = parsedJson.type + parsedJson.attachmentOwnerID + "_" + parsedJson.mediaID;
                    parsedJson.attachmentsPost += parsedJson.attachmentsPostI + ",";
                }
                if (parsedJson.attachmentsPost.charAt(parsedJson.attachmentsPost.length() - 1) == ',') {
                    parsedJson.attachmentsPost = parsedJson.attachmentsPost.substring(0, parsedJson.attachmentsPost.length() - 1);
                    Log.d(LOG_TAG, "attachmentsPost = " + parsedJson.attachmentsPost);
                }
            } else {
                parsedJson.attachmentsPost = null;
            }
            JSONArray copyHistory = json.optJSONArray("copy_history");
            if (copyHistory != null) {
                JSONObject copyHistoryI = copyHistory.getJSONObject(0);
                parsedJson.historyPostID = Integer.parseInt(copyHistoryI.getString("id"));
                parsedJson.historyPostOwnerID = Integer.parseInt(copyHistoryI.getString("owner_id"));
                parsedJson.isRepost = true;
            } else {
                parsedJson.isRepost = false;
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Array is empty");
            e.printStackTrace();
            throw e;
        }
        return parsedJson;
    }

    public String toString(){
        String s = "isRepost:"+isRepost+";historyPostID:"+historyPostID+";historyPostOwnerID:"+historyPostOwnerID+";postID:"+postID+";attachmentPostI:"+attachmentsPostI
                +";message:"+message+";type:"+type+";mediaID:"+mediaID;
        return s;
    }
}
