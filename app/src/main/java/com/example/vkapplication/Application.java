package com.example.vkapplication;

import com.vk.sdk.VKSdk;

/**
 * Created by А on 07.06.2016.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        VKSdk.initialize(this);
    }
}
