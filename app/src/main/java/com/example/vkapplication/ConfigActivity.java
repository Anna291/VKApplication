package com.example.vkapplication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.apache.http.client.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * Created by А on 06.06.2016.
 */
public class ConfigActivity extends Activity {
    public final String LOG_TAG = "VKConfigActivity";
    public String fileAvatarPath;
    LinkedList<ParsedJson> parsedJsons = new LinkedList<ParsedJson>();
    int postsCount = 0;
    int tmpUserID;
    String userIDOrName;

    boolean isAvatarCopying = false;
    boolean isWallCopying = false;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.layout_config);
        Button btnEnter = (Button) findViewById(R.id.btnEnter);
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edtUserID = (EditText) findViewById(R.id.edtUserID);
                userIDOrName = edtUserID.getText().toString();
                if (userIDOrName.isEmpty()){
                    Toast.makeText(ConfigActivity.this, "User ID must be specified", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!((CheckBox)findViewById(R.id.chbAvatar)).isChecked() && !((CheckBox)findViewById(R.id.chbWall)).isChecked()){
                    Toast.makeText(ConfigActivity.this, "Choose data to copy", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (isAvatarCopying || isWallCopying){
                    Toast.makeText(ConfigActivity.this, "Already in progress", Toast.LENGTH_SHORT).show();
                    return;
                }
                getIDAndStartCopy(userIDOrName);
            }
        });
        Button btnDelete = (Button) findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteWall();
            }
        });
        Button btnPageViewFrom = (Button) findViewById(R.id.btnPageViewFrom);
        btnPageViewFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edtUserID = (EditText) findViewById(R.id.edtUserID);
                userIDOrName = edtUserID.getText().toString();
                if (userIDOrName.isEmpty()){
                    Toast.makeText(ConfigActivity.this, "User ID must be specified", Toast.LENGTH_SHORT).show();
                    return;
                }
                viewCopiedID(userIDOrName);
            }
        });
        Button btnPageViewTo = (Button) findViewById(R.id.btnPageViewTo);
        btnPageViewTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewMyID();
            }
        });
    }

    public void viewPage(String usID){
        String url = "http://m.vk.com/"+usID;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void viewMyID(){
        VKRequest request = VKApi.users().get();
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) throws JSONException {
                String myName = "";
                String myID = "";
                try {
                    JSONArray responseArr = response.json.getJSONArray("response");
                    JSONObject responseFirst = responseArr.getJSONObject(0);
                    myName = responseFirst.getString("id");
                    myID = "id" + Integer.parseInt(myName);
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Json has no field userID");
                }
                viewPage(myID);
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                Toast.makeText(ConfigActivity.this, "Error occured while searching user by id: " + error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void viewCopiedID(String userIDOrName){
        VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, userIDOrName,
                VKApiConst.FIELDS, "id, first_name, last_name"));
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d(LOG_TAG, response.json.toString());
                String userName = "";
                tmpUserID = 0;
                String userID = "";
                try {
                    JSONArray responseArr = response.json.getJSONArray("response");
                    Log.d(LOG_TAG, "responseArr null? " + ((responseArr == null) ? "true" : "false"));
                    JSONObject responseFirst = responseArr.getJSONObject(0);
                    userName = responseFirst.getString("id");
                    tmpUserID = Integer.parseInt(userName);
                    userID = "id" + tmpUserID;
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Json has no field userID");
                }
                viewPage(userID);
            }

            @Override
            public void onError(VKError error) {
                Log.d(LOG_TAG, error.toString());
                Toast.makeText(ConfigActivity.this, "Error occured while searching user by id: " + error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void disableGui(){
        findViewById(R.id.btnEnter).setEnabled(false);
        findViewById(R.id.pbCopying).setVisibility(View.VISIBLE);
    }

    public void enableGuiIfDone(){
        Log.d(LOG_TAG, "enabling gui avatar: "+ isAvatarCopying+"; wall: " +isWallCopying);
        if (isAvatarCopying || isWallCopying){
            return;
        }
        findViewById(R.id.btnEnter).setEnabled(true);
        findViewById(R.id.pbCopying).setVisibility(View.GONE);
    }

    public void getIDAndStartCopy(String userIDOrName){
        disableGui();
        VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, userIDOrName,
                VKApiConst.FIELDS, "id, first_name, last_name"));
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d(LOG_TAG, response.json.toString());
                String userName = "";
                tmpUserID = 0;
                String userID = "";
                try {
                    JSONArray responseArr = response.json.getJSONArray("response");
                    Log.d(LOG_TAG, "responseArr null? " + ((responseArr == null) ? "true" : "false"));
                    JSONObject responseFirst = responseArr.getJSONObject(0);
                    userName = responseFirst.getString("id");
                    tmpUserID = Integer.parseInt(userName);
                    userID = "id" + tmpUserID;
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Json has no field userID");
                }
                CheckBox chbAvatar = (CheckBox) findViewById(R.id.chbAvatar);
                CheckBox chbWall = (CheckBox) findViewById(R.id.chbWall);

                EditText edtWallPostsCount = (EditText) findViewById(R.id.edtWallPostsCount);
                String postsCountStr = edtWallPostsCount.getText().toString();
                postsCount = 0;
                if (postsCountStr.isEmpty()) {
                    postsCount = Integer.parseInt(getResources().getString(R.string.wallPostsDefault));
                } else {
                    postsCount = Integer.parseInt(postsCountStr);
                }
                Log.d(LOG_TAG, "userID: " + userID);
                boolean avatar = chbAvatar.isChecked();
                boolean wall = chbWall.isChecked();
                startCopy(userID, avatar, wall);
            }

            @Override
            public void onError(VKError error) {
                Log.d(LOG_TAG, error.toString());
                Toast.makeText(ConfigActivity.this, "error while searching user by id: " + error.toString(), Toast.LENGTH_LONG).show();
                enableGuiIfDone();
            }
        });
    }

    public void startCopy(String userID, boolean avatar, boolean wall){
        Log.d(LOG_TAG, "startCopy userId:" + userID + "; avatar:" + avatar + "; wall:" + wall + "; postsCount:" + postsCount);
        if (avatar) {
            getAvatar(userID);
        }
        if (wall){
            getWall();
        }
    }

    public void termAvatarCopying(String msg){
        if (msg != null && !msg.isEmpty()){
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }
        isAvatarCopying = false;
        enableGuiIfDone();
    }

    public void getAvatar(String userID){
        isAvatarCopying = true;
        VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, userID,
                VKApiConst.FIELDS, "id, first_name, last_name, photo_max_orig"));
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d(LOG_TAG, response.json.toString());
                String photo = "";
                try {
                    JSONArray responseArr = response.json.getJSONArray("response");
                    JSONObject responseFirst = responseArr.getJSONObject(0);
                    photo = responseFirst.getString("photo_max_orig");
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Json has no field photo");
                    termAvatarCopying("Json has no field photo");
                    return;
                }
                Log.d(LOG_TAG, "photo: " + photo);
                DownloadAvatarTask task = new DownloadAvatarTask(photo);
                task.execute();                                                 // Сохранили фото на устройство
            }

            @Override
            public void onError(VKError error) {
                Log.d(LOG_TAG, error.toString());
                termAvatarCopying("error while getting photo: " + error.toString());
            }
        });
    }

    class DownloadAvatarTask extends AsyncTask<Void, Void, Void> {
        private String sUrl;
        public DownloadAvatarTask(String sUrl){
            this.sUrl = sUrl;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            if (downloadImage(sUrl)){
                getUploadURL();
            }
            return null;
        }
    }

    public boolean downloadImage(String sUrl){
        fileAvatarPath = getFilesDir().getPath()+sUrl.substring(sUrl.lastIndexOf('/')+1, sUrl.length());
        URL url = null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            url = new URL(sUrl);
            InputStream in = null;
            in = new BufferedInputStream(url.openStream());
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1!=(n=in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            FileOutputStream fos = new FileOutputStream(fileAvatarPath);
            fos.write(response);
            fos.close();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            termAvatarCopying("Received malformed url from vkserver");
        } catch (IOException e) {
            e.printStackTrace();
            termAvatarCopying("error while saving avatar");
        }
        return false;
    }

    public void getUploadURL(){
        VKRequest request = VKApi.photos().getOwnerPhotoUploadServer(tmpUserID);
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) throws JSONException {
                JSONObject responseURL = response.json.getJSONObject("response");
                String uploadURL = responseURL.getString("upload_url");
                UploadAvatarTask task = new UploadAvatarTask(uploadURL);
                task.execute();
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                termAvatarCopying("error while getting uploading server link: " + error.toString());
            }
        });
    }

    class UploadAvatarTask extends AsyncTask<Void, Void, Void> {
        private String uploadURL;
        public UploadAvatarTask(String uploadURL){
            this.uploadURL = uploadURL;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            uploadImage(uploadURL);
            return null;
        }
    }

    public void uploadImage(String uploadURL){
        String sUrl = uploadURL;
        File file = new File("", fileAvatarPath);
        if (!file.exists()){
            Log.e(LOG_TAG, "file does not exist");
            termAvatarCopying("file does not exist");
            return;
        }

        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1*1024*1024;

        HttpURLConnection conn = null;
        DataInputStream inStream = null;

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            URL url = new URL(sUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"photo\";filename=\"" + fileAvatarPath +"\"" + lineEnd);
            dos.writeBytes(lineEnd);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0){
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String respStr = "";
            String line;
            BufferedReader rd = null;
            while ((line = in.readLine()) != null) {
                respStr += line;
            }
            // close streams
            fileInputStream.close();
            dos.flush();
            dos.close();
            Log.d(LOG_TAG, "upload image responseEntityContent: " + respStr);
            file.delete();
            JSONObject response = new JSONObject(respStr);
            int server = response.getInt("server");
            String photo = response.getString("photo");
            String hash = response.getString("hash");

            savePhotoOnPage(server, photo, hash);

        } catch (MalformedURLException ex){
            ex.printStackTrace();
            termAvatarCopying("Malformed url while uploading avatar");
        } catch (IOException ioe){
            ioe.printStackTrace();
            termAvatarCopying("IOError while uploading avatar");
        } catch (JSONException e) {
            e.printStackTrace();
            termAvatarCopying("Json error while uploading avatar");
        }
    }

    public void savePhotoOnPage(int server, String photo, String hash){
        VKRequest request = VKApi.photos().saveOwnerPhoto(VKParameters.from(VKApiConst.SERVER, server,
                VKApiConst.PHOTO, photo, VKApiConst.HASH, hash));
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) throws JSONException {
                Log.d(LOG_TAG, "Avatar Uploded");
                termAvatarCopying("Avatar was uploaded");
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                Log.d(LOG_TAG, "error in savePhotoOnPage: " + error.toString());
                termAvatarCopying("error while saving photo on page: " + error.toString());
            }
        });
    }

    public void getInfo(String userID){
        VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, userID,
                VKApiConst.FIELDS, "id, first_name, last_name, about"));
        request.secure = false;
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d(LOG_TAG, response.json.toString());
                String about = "";
                try {
                    JSONArray responseArr = response.json.optJSONArray("response");
                    Log.d(LOG_TAG, "responseArr null? " + ((responseArr == null) ? "true" : "false"));
                    JSONObject responseFirst = responseArr.getJSONObject(0);
                    about = responseFirst.getString("about");
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Json has no field about");
                }
                Log.d(LOG_TAG, "about: " + about);
            }

            @Override
            public void onError(VKError error) {
                Log.d(LOG_TAG, error.toString());
            }
        });
    }

    public void termWallCopying(String msg){
        if (msg != null && !msg.isEmpty()){
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }
        isWallCopying = false;
        enableGuiIfDone();
    }

    public void getWall(){
        isWallCopying = true;
        VKRequest reqGetWallCount = VKApi.wall().get(VKParameters.from(VKApiConst.OWNER_ID, tmpUserID,
                VKApiConst.FILTER, "owner", VKApiConst.COUNT, 1));
        execVkRequest(reqGetWallCount, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) throws JSONException {
                Log.d(LOG_TAG, response.json.toString());
                try {
                    JSONObject responseObj = response.json.getJSONObject("response");
                    int wallPostsCount = responseObj.getInt("count");
                    if (postsCount >= wallPostsCount) {
                        postsCount = wallPostsCount;
                    }
                    nextGet();
                } catch (Exception e) {
                    e.printStackTrace();
                    termWallCopying("error parsing wall posts count");
                }
            }

            @Override
            public void onError(VKError error) {
                Log.e(LOG_TAG, "error occured: " + error.toString());
                termWallCopying("error while getting wall posts count: " + error.toString());
            }
        });
    }

    public void nextGet() {
        if (postsCount <= 0){
            termWallCopying("Wall posts uploaded");
            return;
        }
        int offset = 0;
        int reqCount = postsCount;
        if (postsCount > 100){
            reqCount = 100;
            offset = postsCount - reqCount;
        }
        postsCount -= 100;
        VKRequest request = VKApi.wall().get(VKParameters.from(VKApiConst.OWNER_ID, tmpUserID,
                VKApiConst.OFFSET, offset, VKApiConst.COUNT, reqCount, VKApiConst.FILTER, "owner"));
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) throws JSONException {
                Log.d(LOG_TAG, response.json.toString());
                try {
                    JSONObject responseObj = response.json.getJSONObject("response");
                    JSONArray responseItems = responseObj.getJSONArray("items");
                    int length = responseItems.length();
                    for (int i = 0; i < length; i++) {
                        Log.d(LOG_TAG, "i= " + i);
                        JSONObject responseI = responseItems.getJSONObject(i);
                        ParsedJson parsedJson = ParsedJson.fromJson(responseI);
                        if (parsedJson != null) {
                            parsedJsons.add(parsedJson);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    termWallCopying("Error while parsing posts");
                }
                nextPost();
            }

            @Override
            public void onError(VKError error) {
                Log.e(LOG_TAG, "error occured while getting posts: " + error.toString());
                termWallCopying("error occured while getting posts: " + error.toString());

            }
        });
    }

    public void nextPost() {
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ParsedJson parsedJson = parsedJsons.pollLast();
        if (parsedJson == null) {
            nextGet();
            return;
        }
        Log.d(LOG_TAG, "post: "+parsedJson.toString());
        if (parsedJson.isRepost) {
            VKRequest repost = VKApi.wall().repost(VKParameters.from(VKApiConst.OBJECT, "wall" + parsedJson.historyPostOwnerID + "_"
                    + parsedJson.historyPostID, VKApiConst.MESSAGE, parsedJson.message));
            execVkRequest(repost, new OnPostedListener(false));
            return;
        }
        if (parsedJson.attachmentsPost == null) {
            VKRequest post = VKApi.wall().post(VKParameters.from(VKApiConst.MESSAGE, parsedJson.message));
            execVkRequest(post, new OnPostedListener(true));
            return;
        }
        VKRequest post = VKApi.wall().post(VKParameters.from(VKApiConst.MESSAGE, parsedJson.message,
                VKApiConst.ATTACHMENTS, parsedJson.attachmentsPost));
        execVkRequest(post, new OnPostedListener(true));
    }

    public void deleteWall(){
        VKRequest request = VKApi.wall().get(VKParameters.from(VKApiConst.COUNT, 100));
        execVkRequest(request, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) throws JSONException {
                Log.d(LOG_TAG, response.json.toString());
                try {
                    JSONObject responseObj = response.json.getJSONObject("response");
                    JSONArray responseItems = responseObj.getJSONArray("items");
                    int length = responseItems.length();
                    for (int i = 0; i < length; i++) {
                        Log.d(LOG_TAG, "i= " + i);
                        JSONObject responseI = responseItems.getJSONObject(i);
                        ParsedJson parsedJson = ParsedJson.fromJson(responseI);
                        VKRequest deleteReq = VKApi.wall().delete(VKParameters.from(VKApiConst.POST_ID, parsedJson.postID));
                        deleteReq.secure = false;
                        deleteReq.useSystemLanguage = false;
                        deleteReq.executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) throws JSONException {
                                super.onComplete(response);
                            }

                            @Override
                            public void onError(VKError error) {
                                super.onError(error);
                            }
                        });
                    }
                } catch (Exception e) {
                    Log.e(LOG_TAG, "Array is empty");
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VKError error) {
                Log.e(LOG_TAG, "error occured111");
            }
        });
    }

    public void execVkRequest(VKRequest request, VKRequest.VKRequestListener listener){
        request.secure = false;
        request.useSystemLanguage = false;
        request.executeWithListener(listener);
    }

    class OnPostedListener extends VKRequest.VKRequestListener{
        private boolean stopOnError;

        public OnPostedListener(boolean stopOnError){
            this.stopOnError = stopOnError;
        }

        @Override
        public void onComplete(VKResponse response) {
            Log.d(LOG_TAG, "Posted");
            nextPost();
        }

        @Override
        public void onError(VKError error) {
            Log.d(LOG_TAG, String.format("code %d", error.errorCode)+" - "+error.toString());
            if (error.errorCode == -101) {/*Too many requests per second*/
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                execVkRequest(error.request, new OnPostedListener(stopOnError));
                return;
            }
            if (this.stopOnError){
                termWallCopying("Error occured while posting: " + error.toString());
            }else {
                nextPost();
            }
        }
    }
}
