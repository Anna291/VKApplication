package com.example.vkapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

public class MainActivity extends AppCompatActivity {
    public static final String LOG_TAG = "VKAPP_ACTIVITY";
    private static final String[] sMyScope = new String[]{
            VKScope.FRIENDS,
            VKScope.WALL,
            VKScope.PHOTOS,
            VKScope.NOHTTPS,
            VKScope.MESSAGES,
            VKScope.DOCS,
            VKScope.NOTIFY,
            VKScope.GROUPS,
            VKScope.AUDIO,
            VKScope.NOTES
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        Log.d(LOG_TAG, "vklogin");
        VKSdk.login(this, sMyScope);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKCallback<VKAccessToken> callback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                Log.d(LOG_TAG, "vkLoginActivityOnResult");
                // User passed Authorization
                startConfigActivity();
            }

            @Override
            public void onError(VKError error) {
                Log.d(LOG_TAG, "vkLoginActivityOnError");
                finish();
            }
        };

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void startConfigActivity(){
        Log.d(LOG_TAG, "startConfigActivity");
        startActivity(new Intent(this, ConfigActivity.class));
        finish();
    }
}
